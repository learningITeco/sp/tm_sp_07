<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<body>

	<div class="container">

		<c:if test="${not empty msg}">
		    <div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		    </div>
		</c:if>

		<h1>All Projects</h1>

        <spring:url value="/projects/add" var="addUrl" />
        <button class="btn btn-primary" onclick="location.href='${addUrl}'">Add</button>

		<table class="table table-striped">
			<thead>
				<tr>
				    <th>No.</th>
					<th>#ID</th>
					<th>NAME</th>
					<th>DESCRIPTION</th>
					<th>USER</th>
					<th>DATE START</th>
					<th>DATE FINISH</th>
					<th>STATUS</th>
				</tr>
			</thead>

			<c:forEach var="project" items="${projectList}" varStatus="status">
			    <tr>
                    <td align="center">${status.count}</td>
                    <td><input name="project[${status.index}].id" value="${project.id}"/></td>
                    <td><input name="project[${status.index}].name" value="${project.name}"/></td>
                    <td><input name="project[${status.index}].description" value="${project.description}"/></td>
                    <td><input name="project[${status.index}].user.getLogin()" value="${project.user.getLogin()}"/></td>
                    <td><input name="project[${status.index}].dateStart" value="${project.dateStart}"/></td>
                    <td><input name="project[${status.index}].dateFinish" value="${project.dateFinish}"/></td>
                    <td><input name="project[${status.index}].status" value="${project.status}"/></td>

                    <td>
                      <spring:url value="/projects/${project.id}/delete" var="deleteUrl" />
                      <spring:url value="/projects/${project.id}/update" var="updateUrl" />

                      <button class="btn btn-primary"
                                              onclick="location.href='${updateUrl}'">Update</button>
                      <button class="btn btn-danger"
                                              onclick="location.href='${deleteUrl}'">Delete</button>
                    </td>
			    </tr>
			</c:forEach>
		</table>

	</div>
</body>
</html>