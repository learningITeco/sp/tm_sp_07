<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<div class="container">

	<c:choose>
		<c:when test="${userForm['new']}">
			<h1>Add User</h1>
		</c:when>
		<c:otherwise>
			<h1>Update User</h1>
		</c:otherwise>
	</c:choose>
	<br />

	<spring:url value="/users" var="userActionUrl" />

	<form:form class="form-horizontal" method="post"
                modelAttribute="userForm" action="${userActionUrl}">

		<form:hidden path="id" />

		<spring:bind path="login">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
			<label class="col-sm-2 control-label">Login</label>
			<div class="col-sm-10">
				<form:input path="login" type="text" class="form-control"
                                id="login" placeholder="Login" />
				<form:errors path="login" class="control-label" />
			</div>
		  </div>
		</spring:bind>

		<spring:bind path="roleType">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
			<label class="col-sm-2 control-label">RoleType</label>
			<div class="col-sm-10">

                <form:select path="roleType" multiple="false" name="roleType">
                    <c:forEach items="${roles}" var="role">
                        <form:option value="${role}">${role}</form:option>
                    </c:forEach>
                </form:select>

				<form:errors path="roleType" class="control-label" />
			</div>
		  </div>
		</spring:bind>

		<spring:bind path="hashPass">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
			<label class="col-sm-2 control-label">HashPass</label>
			<div class="col-sm-10">
				<form:input path="hashPass" class="form-control"
                                id="hashPass" placeholder="HashPass" />
				<form:errors path="hashPass" class="control-label" />
			</div>
		  </div>
		</spring:bind>

		<div class="form-group">
		  <div class="col-sm-offset-2 col-sm-10">
			<c:choose>
			  <c:when test="${userForm['new']}">
			     <button type="submit" class="btn-lg btn-primary pull-right">Add</button>
			  </c:when>
			  <c:otherwise>
			     <button type="submit" class="btn-lg btn-primary pull-right">Update</button>
			  </c:otherwise>
			</c:choose>
		  </div>
		</div>
	</form:form>

</div>

</body>
</html>