<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<body>

	<div class="container">

		<c:if test="${not empty msg}">
		    <div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		    </div>
		</c:if>

		<h1>All Users</h1>

        <spring:url value="/users/add" var="addUrl" />
        <button class="btn btn-primary" onclick="location.href='${addUrl}'">Add</button>

		<table class="table table-striped">
			<thead>
				<tr>
				    <th>No.</th>
					<th>#ID</th>
					<th>LOGIN</th>
					<th>HASHPASS</th>
					<th>ROLE</th>
				</tr>
			</thead>

			<c:forEach var="user" items="${userList}" varStatus="status">
			    <tr>
                    <td align="center">${status.count}</td>
                    <td><input name="user[${status.index}].id" value="${user.id}"/></td>
                    <td><input name="user[${status.index}].login" value="${user.login}"/></td>
                    <td><input name="user[${status.index}].hashPass" value="${user.hashPass}"/></td>
                    <td><input name="user[${status.index}].roleType" value="${user.roleType}"/></td>

                    <td>
                      <spring:url value="/users/${user.id}" var="userUrl" />
                      <spring:url value="/users/${user.id}/delete" var="deleteUrl" />
                      <spring:url value="/users/${user.id}/update" var="updateUrl" />

                      <button class="btn btn-info"
                                              onclick="location.href='${userUrl}'">Query</button>
                      <button class="btn btn-primary"
                                              onclick="location.href='${updateUrl}'">Update</button>
                      <button class="btn btn-danger"
                                              onclick="location.href='${deleteUrl}'">Delete</button>
                    </td>
			    </tr>
			</c:forEach>
		</table>

	</div>
</body>
</html>