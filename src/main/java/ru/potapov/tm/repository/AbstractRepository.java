package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Setter
@NoArgsConstructor
@Repository
@Transactional
public abstract class AbstractRepository <T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void merge(@NotNull T t) {
        entityManager.merge( entityManager.contains(t) ? t : entityManager.merge(t) );
    }

    @Override
    public void persist(@NotNull T t) {
        entityManager.persist( entityManager.contains(t) ? t : entityManager.merge(t) );
    }

    @Override
    public void remove(@NotNull T t) {
        entityManager.remove( entityManager.contains(t) ? t : entityManager.merge(t) );
    }


}
