package ru.potapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.User;

import javax.faces.bean.ManagedBean;
import javax.inject.Named;

@Repository
//@Named(value = "iProjectRepository")
//@ManagedBean(name = "iProjectRepository", eager = true)
public interface IProjectRepository extends CrudRepository<Project, String> {

    @NotNull Iterable<Project> findAllByUser(@NotNull final User user);

    @Nullable Project findByName(@NotNull final String name);

    @Query("select e from Project e where e.name = ?1 and user = ?2")
    @Nullable Project findProjectByNameAndByUser(@NotNull final String name, @NotNull final User user);

    @Query("select e from Project e where e.id = ?1 and user = ?2")
    @Nullable Project findProjectByIdAndByUser(@NotNull final String id, @NotNull final User user);
}
