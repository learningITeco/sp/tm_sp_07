package ru.potapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.AbstractEntity;
import ru.potapov.tm.enumeration.RoleType;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public class UserDto implements Cloneable, Serializable {
    @Nullable private String    id;
    @Nullable private String    login;
    @Nullable private String    hashPass;
    @Nullable private String    sessionId;
    @Nullable private RoleType   roleType;
}
