package ru.potapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.AbstractEntity;

@Getter
@Setter
@NoArgsConstructor
public class SessionDto {
    @Nullable private String id;
    @Nullable private String userId;
    @Nullable private String signature;
    @Nullable private long dateStamp;
}
