package ru.potapov.tm.util;

import org.jetbrains.annotations.NotNull;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class JsfUtil {
    public static void reload() throws IOException {
        @NotNull ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }
}
