package ru.potapov.tm.util;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.format.FormatterRegistry;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.formatter.ProjectFormatter;
import ru.potapov.tm.formatter.RoleFormatter;
import ru.potapov.tm.formatter.StatusFormatter;
import ru.potapov.tm.formatter.UserFormatter;

import java.util.Collection;

@Setter
@Getter
@EnableWebMvc
@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "ru.potapov.tm")
@EnableJpaRepositories("ru.potapov.tm.repository")
public class WebMvcConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {
    @Autowired
    @NotNull private ServiceLocator serviceLocator;

    @Autowired
    @NotNull UserDetailsService userService;

    @Bean
    public InternalResourceViewResolver resolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public UrlBasedViewResolver setupViewResolver() {
        UrlBasedViewResolver resolver = new UrlBasedViewResolver();
        // указываем где будут лежать наши веб-страницы
        resolver.setPrefix("/pages/");
        // формат View который мы будем использовать
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);

        return resolver;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new UserFormatter(serviceLocator));
        registry.addFormatter(new ProjectFormatter(serviceLocator));
        registry.addFormatter(new StatusFormatter());
        registry.addFormatter(new RoleFormatter());
    }


    //#############           SECURITY BLOCK  #######################
//    //Plug For security 5
//    @SuppressWarnings("deprecation")
    @Bean
    public static NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }

    @Bean
    public static PasswordEncoder passwordEncoder1() {
        return new BCryptPasswordEncoder();
    }

//    @Bean
//    public UserDetailsService userDetailsService() {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(org.springframework.security.core.userdetails.User.withDefaultPasswordEncoder()
//                .username("admin").password("{noop}1").roles("ADMIN").build());
//        return manager;
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().
                antMatchers("/*").permitAll().
                antMatchers("/secure/users/*").hasAuthority(RoleType.Administrator.toString()).//hasRole(RoleType.Administrator.toString()).
                antMatchers("/secure/*", "/secure/projects/*", "/secure/tasks/*").hasAnyAuthority(RoleType.Administrator.toString(), RoleType.User.toString()).//hasAnyRole(RoleType.Administrator.toString(), RoleType.User.toString()).
                and().formLogin().  //login configuration
                loginPage("/customLogin.xhtml").
                loginProcessingUrl("/appLogin").
                usernameParameter("app_username").
                passwordParameter("app_password").
                //passwordParameter("hashPass").
                defaultSuccessUrl("/index.xhtml");

        // Have to disable it for POST methods:
        // http://stackoverflow.com/a/20608149/1199132
        http.csrf().disable();

        // Logout and redirection:
        // http://stackoverflow.com/a/24987207/1199132
        http.logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/appLogout"))
                .invalidateHttpSession(true)
                .logoutSuccessUrl(
                        "/customLogin.xhtml");

    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userService);
        authProvider.setPasswordEncoder(passwordEncoder1());
        return authProvider;
    }

    @Autowired
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
//        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
//        authenticationProvider.setUserDetailsService(userService);
//        authenticationProvider.setPasswordEncoder(passwordEncoder());
//        auth.authenticationProvider(authenticationProvider);
    }

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//
//        //Configure roles and passwords as in-memory authentication
//        Collection<User> listUsers = serviceLocator.getUserService().findAll();
//        for (User user : listUsers) {
//            auth.inMemoryAuthentication()
//                    .withUser(user.getLogin())
//                    .password(user.getPassword())
//                    .roles(user.getRoleType().toString());
//        }
//    }
}
