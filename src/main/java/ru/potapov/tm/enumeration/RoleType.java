package ru.potapov.tm.enumeration;

public enum RoleType {
    Administrator,
    Manager,
    User;
}
