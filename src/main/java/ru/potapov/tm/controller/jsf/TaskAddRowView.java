package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.enumeration.Status;
import ru.potapov.tm.util.JsfUtil;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ManagedBean(name = "dtTaskAddRowView", eager = true)
@RequestScoped
public class TaskAddRowView extends SpringBeanAutowiringSupport implements Serializable {
    @Nullable
    private List<Task> tasks;

    @Autowired
    @NotNull
    ITaskService taskService;

    @PostConstruct
    public void init() {
        tasks = (List<Task>) taskService.findAll();
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public List<Status> getStatuses(){
        return Arrays.asList(Status.values());
    }

    public void onRowEdit(RowEditEvent event) {
        @NotNull Task task = (Task)event.getObject();
        taskService.merge(task);
        @NotNull FacesMessage msg = new FacesMessage("Task Edited", task.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        @NotNull Task task = (Task)event.getObject();
        @NotNull FacesMessage msg = new FacesMessage("Edit Cancelled", task.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onAddNew() {
        // Add one new task to the table:
        @NotNull Task task2Add = new Task();
        task2Add.setName("New task" + Math.round(Math.random()*100));
        task2Add.setDescription("Set description there");
        task2Add.setStatus(Status.Planned);
        task2Add.setDateStart(new Date());
        task2Add.setDateFinish(new Date());

        taskService.merge(task2Add);

        @NotNull FacesMessage msg = new FacesMessage("New Task added", task2Add.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);

        try {
            JsfUtil.reload();
        }catch (IOException e){ e.printStackTrace(); }
    }

}
