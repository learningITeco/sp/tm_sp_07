package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ManagedBean(name = "dtUserAddRowView", eager = true)
@RequestScoped
public class UserAddRowView extends SpringBeanAutowiringSupport implements Serializable {
    @Nullable
    private List<User> users;

    @Autowired
    @NotNull
    IUserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Nullable String authUser;

    @PostConstruct
    public void init() {
        users = (List<User>) userService.findAll();
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Status> getStatuses(){
        return Arrays.asList(Status.values());
    }

    public void onRowEdit(RowEditEvent event) {
        @NotNull User user = (User)event.getObject();
        userService.merge(user);
        @NotNull FacesMessage msg = new FacesMessage("User Edited", user.getLogin());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        @NotNull User user = (User)event.getObject();
        @NotNull FacesMessage msg = new FacesMessage("Edit Cancelled", user.getLogin());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onAddNew() {
        // Add one new car to the table:
        @NotNull User user2Add = new User();
        user2Add.setLogin("New user" + Math.round(Math.random()*100));
        user2Add.setPassword("$2a$10$o0/vzrMq3eiLmaXrnMKucebabooPxdvuyyekyBSPd1JbFcUcbDhxO");
        user2Add.setPassword(passwordEncoder.encode("1"));

        user2Add.setRoleType(RoleType.User);

        userService.merge(user2Add);

        @NotNull FacesMessage msg = new FacesMessage("New User added", user2Add.getLogin());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String getAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            authUser = authentication.getName();
            return authUser;
        }
        return null;
    }
}
