package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.util.JsfUtil;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ManagedBean(name = "dtUserEditView", eager = true)
@ViewScoped
public class UserEditView extends SpringBeanAutowiringSupport implements Serializable {
    @Nullable private List<User> users;
    @Nullable private List<User> users2;
    @Nullable private List<RoleType> roleTypes;
    @Nullable private User selectedUser;

    @Autowired
    @NotNull
    IUserService userService;

    @PostConstruct
    public void init() {
        users = (List<User>) userService.findAll();
        users2 = (List<User>) userService.findAll();
        users = (List<User>) userService.findAll();
        roleTypes = getRoles();
    }

    public List<User> getUsers() {
        return users;
    }

    public List<User> getUsers2() {
        return users2;
    }

    public List<RoleType> getRoles(){
        return Arrays.asList(RoleType.values());
    }

    public void onCellEdit(CellEditEvent event) {
        @NotNull Object oldValue = event.getOldValue();
        @NotNull Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            @NotNull FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        @NotNull User user = (User)event.getObject();
        @NotNull FacesMessage msg = new FacesMessage("User Edited", user.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        @NotNull User user = (User)event.getObject();
        @NotNull FacesMessage msg = new FacesMessage("Edit Cancelled", user.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void deleteUsers() {
        userService.remove(selectedUser);
        @NotNull FacesMessage msg = new FacesMessage("Dlete completed", selectedUser.getLogin());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        selectedUser = null;

        try {
            JsfUtil.reload();
        }catch (IOException e){ e.printStackTrace(); }
    }
}
