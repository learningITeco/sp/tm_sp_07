package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.Status;
import ru.potapov.tm.util.JsfUtil;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ManagedBean(name = "dtTaskEditView", eager = true)
@ViewScoped
public class TaskEditView extends SpringBeanAutowiringSupport implements Serializable {
    @Nullable private List<Task> tasks;
    @Nullable private List<Task> tasks2;
    @Nullable private List<Status> statuses;
    @Nullable private List<User> users;
    @Nullable private List<Project> projects;
    @Nullable private Task selectedTask;

    @Autowired
    @NotNull
    ITaskService taskService;

    @Autowired
    @NotNull
    IProjectService projectService;

    @Autowired
    @NotNull
    IUserService userService;

    @PostConstruct
    public void init() {
        tasks = (List<Task>) taskService.findAll();
        tasks2 = (List<Task>) taskService.findAll();
        users = (List<User>) userService.findAll();
        projects = (List<Project>) projectService.findAll();
        statuses = getStatuses();
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public List<Task> getTasks2() {
        return tasks2;
    }

    public List<Status> getStatuses(){
        return Arrays.asList(Status.values());
    }

    public void onCellEdit(CellEditEvent event) {
        @NotNull Object oldValue = event.getOldValue();
        @NotNull Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            @NotNull FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        @NotNull Task task = (Task)event.getObject();
        @NotNull FacesMessage msg = new FacesMessage("Task Edited", task.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        @NotNull Task task = (Task)event.getObject();
        @NotNull FacesMessage msg = new FacesMessage("Edit Cancelled", task.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void deleteTask() {
        taskService.remove(selectedTask);
        @NotNull FacesMessage msg = new FacesMessage("Dlete completed", selectedTask.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        selectedTask = null;

        try {
             JsfUtil.reload();
        }catch (IOException e){ e.printStackTrace(); }
    }
}
