package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.Status;
import ru.potapov.tm.util.JsfUtil;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
//@Named("dtEditView")
@ManagedBean(name = "dtEditView", eager = true)
@ViewScoped
public class ProjectEditView  extends SpringBeanAutowiringSupport implements Serializable {
    @Nullable private List<Project> projects;
    @Nullable private List<Project> projects2;
    @Nullable private List<Status> statuses;
    @Nullable private List<User> users;

    @Nullable private Project selectedProject;

    @Autowired
    @NotNull
    IProjectService projectService;

    @Autowired
    @NotNull
    IUserService userService;

    @PostConstruct
    public void init() {
        projects = (List<Project>) projectService.findAll();
        projects2 = (List<Project>) projectService.findAll();
        users = (List<User>) userService.findAll();
        statuses = getStatuses();
    }

    public List<User> getUsers() {
        return users;
    }

    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public List<Project> getProjects2() {
        return projects2;
    }

    public List<Status> getStatuses(){
        return Arrays.asList(Status.values());
    }

    public void onCellEdit(CellEditEvent event) {
        @Nullable Object oldValue = event.getOldValue();
        @Nullable Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            @NotNull FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        @NotNull final Project project = (Project)event.getObject();
        @NotNull FacesMessage msg = new FacesMessage("Project Edited", project.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        @NotNull final Project project = (Project)event.getObject();
        @NotNull FacesMessage msg = new FacesMessage("Edit Cancelled", project.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void deleteProject() {
        projectService.remove(selectedProject);
        @NotNull FacesMessage msg = new FacesMessage("Dlete completed", selectedProject.getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        selectedProject = null;

        try {
            JsfUtil.reload();
        }catch (IOException e){ e.printStackTrace(); }
    }
}
