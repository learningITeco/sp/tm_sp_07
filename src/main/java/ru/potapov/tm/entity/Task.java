package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.potapov.tm.enumeration.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_task")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractEntity implements Cloneable, Serializable {

    @Id
    @Nullable
    private String id;

    @Column(name = "name", unique = true, nullable = false)
    @Nullable
    private String      name;

    @Column(name = "description")
    @Nullable private String      description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @Nullable private User  user;

    @Column(name = "dateBegin")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Nullable private Date dateStart;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(name = "dateEnd")
    @Nullable private Date dateFinish;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    @Nullable private Status status;

    public Task() {
        id = UUID.randomUUID().toString();
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="project_id")
    @Nullable
    private Project      project;

    //Check if this is for New of Update
    public boolean isNew() {
        return (getName().isEmpty());
    }

}
