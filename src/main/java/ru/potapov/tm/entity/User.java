package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.potapov.tm.enumeration.RoleType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Setter
@Getter
@Entity
@Table(name = "app_user")
public class User extends AbstractEntity implements Cloneable, Serializable, UserDetails {
    @Id
    @Nullable
    private String id;

    @Column(nullable = false, unique = true)
    @Nullable private String login;

//    @Column(name = "passwordHash")
//    @Nullable private String hashPass;

    @Column(name = "password")
    @Nullable private String password = "";

    @Column(name = "session_id")
    @Nullable private String sessionId;

    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    @NotNull private RoleType roleType = RoleType.User;

    public User() {
        id = UUID.randomUUID().toString();
    }

    //Check if this is for New of Update
    public boolean isNew() {
        return (getLogin().isEmpty());
    }

    @Override
    public String toString() {
        return login + " [" + roleType + "]";
    }

    @Override
    public String getName() {
        return getLogin();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        // add user's authorities
        setAuths.add(new SimpleGrantedAuthority(roleType.toString()));

        return setAuths;
    }

    @Override
    public String getUsername() {
        return getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
