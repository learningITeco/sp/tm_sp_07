package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.UserDto;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;

import java.util.Collection;

public interface IUserService extends IService<User, UserDto> {
    @NotNull User createUser(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role);
    public void createPredefinedUsers();
}
