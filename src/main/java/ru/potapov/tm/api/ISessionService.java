package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.SessionDto;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;

public interface ISessionService extends IService<Session, SessionDto> {
    @Nullable Session generateSession(@NotNull Session session, @NotNull final String userId);
}
