package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();
    @NotNull ITaskService getTaskService();
    @NotNull ISessionService getSessionService();
    @NotNull IUserService getUserService();
    void init();
}
