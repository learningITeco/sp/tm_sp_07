package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.Collection;

public interface IRepository<T extends AbstractEntity> {
    @Nullable T findOne(@NotNull final String id);
    @NotNull Collection<T> findAll();
    void merge(@NotNull T t);
    void persist(@NotNull T t);
    void remove(@NotNull T t);
    void removeAll();

    @Nullable EntityManager getEntityManager();
    void setEntityManager(EntityManager entityManager);
}
